﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;

namespace Event_planning.Services
{
    public class EmailSender : IEmailSender
    {
        public Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Events planing site admin", "eventsplanings@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("Events planing", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 25, false);
                client.Authenticate("eventsplanings@gmail.com", "eventplaning");
                client.Send(emailMessage);
                client.Disconnect(true);
            }
            return Task.CompletedTask;
        }
    }
}
