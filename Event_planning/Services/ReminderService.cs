﻿using Event_planning.Data;
using Event_planning.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Event_planning.Services
{
    public class ReminderService
    {
        public async Task SendReminderEmailsAsync(ApplicationDbContext context)
        {
            EmailSender sender = new EmailSender();

            await Task.Run(async () => {while (true)
                {
                    List<Event> events = context.Events.Include(x => x.EventApplicationUsers).ThenInclude(x => x.ApplicationUserId).
                    Where(ev =>  DateTime.Now.AddDays(1) > ev.DateTime).ToList();
                    try {
                        foreach (var evnt in events)
                        {
                            var users = evnt.EventApplicationUsers.Select(sc => sc.ApplicationUser).ToList();
                            if (users.Any())
                            {
                                foreach (var user in users)
                                {
                                    await sender.SendEmailAsync(user.Email, evnt.Title, Convert.ToString(evnt.DateTime)); 
                                }
                            }               
                        }
                        Thread.Sleep(new TimeSpan(1, 0,  0));
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(new TimeSpan(0, 10, 0));
                    }
                }
            });
        }
    }
}
