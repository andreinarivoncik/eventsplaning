﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Event_planning.Data;
using Microsoft.EntityFrameworkCore;

using Event_planning.Models;

namespace Event_planning.Controllers
{
    [Authorize(Roles = "sysadmin")]
    public class AdminController : Controller
    {
        private ApplicationDbContext appDbContext;

        public AdminController(ApplicationDbContext _context)
        {
            appDbContext = _context;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            Console.Beep();
            ApplicationUser user = appDbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            ViewBag.email = user.Email;
            ViewBag.name = user.Name;
            ViewBag.age = user.Age;
            return View();
        }

        [HttpGet]
        public IActionResult EventsManagment()
        {
            try
            {
                IEnumerable<Event> events = appDbContext.Events;
                return View(events);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpGet]
        public IActionResult UsersManagment()
        {
            try
            {
                IEnumerable<ApplicationUser> users = appDbContext.Users.Where(u => u.UserName != User.Identity.Name);
                return View(users);
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }            
        }

        [HttpPost]
        public async Task<IActionResult> UsersManagment(IEnumerable<string> users_id)
        {
            try
            {
                foreach (var id in users_id)
                {
                    ApplicationUser user = await appDbContext.Users.FirstOrDefaultAsync(u => u.Id == id);
                    if (user != null)
                    {
                        appDbContext.Users.Remove(user);
                    }
                }
                await appDbContext.SaveChangesAsync();
                return RedirectToAction("UsersManagment");
            }
            catch (Exception)
            {
                Console.Beep(400, 40);
                return RedirectToAction("UsersManagment");
            }
        }

        [HttpGet]
        public IActionResult EventCreate()
        {
            return RedirectToAction(actionName: "EventCreate", controllerName: "Events");
        }

        [HttpGet]
        public IActionResult EventsList()
        {
            return RedirectToAction(actionName: "EventsList", controllerName: "Events");
        }

        [HttpGet]
        public IActionResult UsersList()
        {
            return RedirectToAction(actionName: "UsersList", controllerName: "Users");
        }
    }
}