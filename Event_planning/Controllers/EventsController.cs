﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Event_planning.Data;
using Event_planning.Models;
using Microsoft.AspNetCore.Authorization;
using Event_planning.Models.ViewModels;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Event_planning.Models.ForEventDataModels;

namespace Event_planning.Controllers
{
    public class EventsController : Controller
    {
        private ApplicationDbContext appDbContext;

        public EventsController(ApplicationDbContext _appDbContext)
        {
            appDbContext = _appDbContext;
        }
        
        [HttpGet]
        [Authorize(Roles = "sysadmin")]
        public ActionResult EventCreate()
        { 
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "sysadmin")]
        public ActionResult EventCreate(string name, string date, string count, List<string> NameDescription, List<string> Description)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(date);
                Event evnt = new Event { CurUsersCount = 0, MaxUsersCount = int.Parse(count), Title = name, DateTime = DateTime.Now };
                if ((NameDescription != null) && (Description != null))
                {
                    for (int i = 0; i < NameDescription.Count; i++)
                    {
                        evnt.OtherEventInfos.Add(new OtherEventInfo { HeadInfo = NameDescription[i], TextInfo = Description[i] });
                    }
                }
                appDbContext.Events.Add(evnt);//.Event);
                appDbContext.SaveChanges();
                return RedirectToAction("Index", "Admin");
            }
            catch(Exception e)
            {
                return Content(e.Message);
            }
        }

        [HttpGet]
        [Authorize(Roles = "sysadmin, user")]
        public ActionResult EventDetails(int id)
        {
            Event ev = appDbContext.Events.Include(u=>u.OtherEventInfos).FirstOrDefault(evnt => evnt.Id == id);
            if (ev != null)
            {
                List<OtherEventInfo> infos = appDbContext.OtherEventInfos.Where(inf => inf.EventId == id).ToList();
                EventContext ec = new EventContext { Event = ev, EventInfos = infos };
                return View(ec);
            }
            else
                return NoContent();
        }

        [HttpGet]
        [Authorize(Roles = "sysadmin")]
        public ActionResult EventEdit(int id)
        {
            Event ev = appDbContext.Events.FirstOrDefault(evnt => evnt.Id == id);
            if (ev != null)
            {
                return View(ev);
            }
            else
                return NotFound();
        }

        [HttpPost]
        [Authorize(Roles = "sysadmin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EventEdit(Event ev)
        {
            try
            {
                appDbContext.Events.Update(ev);
                await appDbContext.SaveChangesAsync();
                return RedirectToAction("EventsManagment", "Admin");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        [Authorize(Roles = "sysadmin")]
        public ActionResult EventDelete(int id)
        {
            Event ev = appDbContext.Events.FirstOrDefault(evnt => evnt.Id == id);
            if (ev != null)
                appDbContext.Events.Remove(ev);
            appDbContext.SaveChanges();
            return RedirectToAction("EventsManagment", "Admin");
        }

        [HttpGet]
        public ActionResult EventsList()
        {
            IEnumerable<Event> events = appDbContext.Events;
            return View(events);
        }

    }
}