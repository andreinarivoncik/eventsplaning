﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Event_planning.Models;
using Event_planning.Services;
using Event_planning.Data;
using Microsoft.AspNetCore.Authorization;

namespace Event_planning.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController(ApplicationDbContext app_context)
        {
            _context = app_context;
        }

        [Authorize(Roles = "sysadmin, user")]
        public IActionResult Index()
        {
            if (User != null)
            {
                if (User.IsInRole("user") && User.Identity.IsAuthenticated)
                {
                    ViewBag.role = "user";
                    return RedirectToAction("Index", "User");
                }
                else if (User.IsInRole("sysadmin") && User.Identity.IsAuthenticated)
                {
                    ViewBag.role = "admin";
                    return RedirectToAction("Index", "Admin");
                }
            }
            return Error();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Event Scheduler is an application that allows a user to register on the site," +
                " view and subscribe to events. The application reminds the user of nearby events.";
 
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Email: andrejnarivoncik@gmail.com.\n Mobile phone: +375291154494";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
