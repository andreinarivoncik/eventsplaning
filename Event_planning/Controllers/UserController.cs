﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Event_planning.Data;
using Event_planning.Models;
using Microsoft.EntityFrameworkCore;

namespace Event_planning.Controllers
{
    [Authorize(Roles = "user")]
    public class UserController : Controller
    {
        private ApplicationDbContext appDbContext;

        public UserController(ApplicationDbContext _appDbContext)
        {
            appDbContext = _appDbContext;
        }

        [HttpGet]
        public IActionResult Index()
        {
            Console.Beep();
            ApplicationUser user = appDbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            ViewBag.email = user.Email;
            ViewBag.name = user.Name;
            ViewBag.age = user.Age;
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "user")]
        public IActionResult SubscribedEvents()
        {
            ApplicationUser user = appDbContext.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            try
            {
                List<Event> selected_events = new List<Event>();
                var events = appDbContext.Events.Include(c => c.EventApplicationUsers).ThenInclude(sc => sc.ApplicationUser).ToList();

                foreach (var ev in events)
                {
                    var users = ev.EventApplicationUsers.Select(sc => sc.ApplicationUser).Where(x => x.Id == user.Id).ToList();
                    if(users.Any())
                        selected_events.Add(ev);
                }

                return View(selected_events);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }

       
        [HttpGet]
        [Authorize(Roles = "user")]
        public IActionResult AllEvents()
        {
            ApplicationUser user = appDbContext.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            try
            {
                List<Event> selected_events = new List<Event>();
                var events = appDbContext.Events.Include(c => c.EventApplicationUsers).ThenInclude(sc => sc.ApplicationUser).ToList();

                foreach (var ev in events)
                {
                    var users = ev.EventApplicationUsers.Select(sc => sc.ApplicationUser).Where(x => x.Id == user.Id).ToList();
                    if (users.Any())
                        selected_events.Add(ev);
                }
            
                IEnumerable<Event> events_col = appDbContext.Events.Except(selected_events).Where(ev => ev.MaxUsersCount != ev.CurUsersCount);
                return View(events_col);
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpGet]
        [Authorize(Roles = "user")]
        public IActionResult Subscribe(int id)
        {
            Event evnt = appDbContext.Events.FirstOrDefault(ev => ev.Id == id);
            ApplicationUser user = appDbContext.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if(user != null)
            {
                EventApplicationUser eventApplicationUser = new EventApplicationUser {ApplicationUserId = user.Id, EventId = id };
                user.EventApplicationUsers.Add(eventApplicationUser);
                evnt.CurUsersCount += 1;
                appDbContext.Events.Update(evnt);
                appDbContext.SaveChanges();
            }
            return RedirectToAction(actionName: "AllEvents", controllerName: "User");
        }

        [HttpGet]
        public IActionResult Unsubscribe(int id)
        {
            ApplicationUser user = appDbContext.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (user != null)
            {
                Event evnt = appDbContext.Events.FirstOrDefault(ev => ev.Id == id);
                var events = appDbContext.Events.Include(c => c.EventApplicationUsers).ThenInclude(sc => sc.ApplicationUser).ToList();
                var selected_binding = new List<EventApplicationUser>();

                foreach (var ev in events)
                {
                    var bindings = ev.EventApplicationUsers.Where(z => (z.EventId == id) && (z.ApplicationUserId == user.Id));
                    if (bindings.Any())
                        selected_binding.AddRange(bindings);
                }

                foreach (var b in selected_binding)
                {
                    user.EventApplicationUsers.Remove(b);
                }
                evnt.CurUsersCount -= 1;
                appDbContext.Events.Update(evnt);
                appDbContext.SaveChanges();
            }
            return RedirectToAction(actionName: "SubscribedEvents", controllerName: "User");
        }
    }
}