﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Event_planning.Data;
using Event_planning.Models;
using Event_planning.Services;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Event_planning
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("UsersDbConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                });

            services.AddTransient<IEmailSender, EmailSender>();
            
            services.AddTransient<ReminderService>();

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ApplicationDbContext context, UserManager<ApplicationUser> manager, ReminderService reminderService)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            ApplicationDbContextInit.Initialize(context);

            //check emailReminder
            Task.Run(()=>reminderService.SendReminderEmailsAsync(context));

            string adminEmail = Configuration.GetSection("Admin")["Email"];
            string adminPassword = Configuration.GetSection("Admin")["Password"];
            
            if(! context.Users.Where(x=>x.Email == adminEmail).ToList().Any())
                Create_admin(context, manager, adminEmail, adminPassword);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private async void  Create_admin(ApplicationDbContext context, UserManager<ApplicationUser> manager,
            string adminEmail, string password)
        {
            ApplicationUser user = new ApplicationUser {UserName=adminEmail, Email = adminEmail, Age=19, Name="Oleg", EmailConfirmed = true};
            context.Users.Add(user);
            context.SaveChanges();
            var role = context.Roles.FirstOrDefault(x => x.Name == "sysadmin");
            await manager.AddPasswordAsync(user, password);
            await manager.AddToRoleAsync(user, role.NormalizedName);
            context.SaveChanges();

            /*ApplicationUser temp_user = new ApplicationUser { UserName = "andreinariwonchik@gmail.com", Email = "andreinariwonchik@gmail.com", EmailConfirmed = true };
            context.Users.Add(temp_user);
            context.SaveChanges();
            role = context.Roles.FirstOrDefault(x => x.Name == "user");
            await manager.AddPasswordAsync(temp_user, password);
            await manager.AddToRoleAsync(temp_user, role.NormalizedName);
            context.SaveChanges();*/
        }
    }
}
