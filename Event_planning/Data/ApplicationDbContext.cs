﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Event_planning.Models;
using Event_planning.Models.ForEventDataModels;

namespace Event_planning.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Event> Events { get; set; }
        public DbSet<OtherEventInfo> OtherEventInfos {get; set;}

        //public DbSet<OtherEventInfo> UserInfos { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //SetCascadeDeletingForOtherEventInfo(modelBuilder);
            SetManyToManyForEventToApplicationUser(modelBuilder);
            SetManyToManyForEventToApplicationUser(modelBuilder);
        }

        private void SetCascadeDeletingForOtherEventInfo(ModelBuilder modelBuilder)
        {
             modelBuilder.Entity<OtherEventInfo>()
            .HasOne(i => i.Event)
            .WithMany(e_i => e_i.OtherEventInfos)
            .OnDelete(DeleteBehavior.Cascade);
        }

        /*private void SetCascadeDeletingForUserInfo(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserInfo>()
           .HasOne(i => i.ApplicationUser)
           .WithOne(e_i => e_i.UserInfo)
           .OnDelete(DeleteBehavior.Cascade);
        }*/

        private void SetManyToManyForEventToApplicationUser(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EventApplicationUser>()
            .HasKey(x => new { x.EventId, x.ApplicationUserId });

            modelBuilder.Entity<EventApplicationUser>()
                .HasOne(ev => ev.Event)
                .WithMany(e => e.EventApplicationUsers)
                .HasForeignKey(e => e.EventId);//.OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<EventApplicationUser>()
                .HasOne(e => e.ApplicationUser)
                .WithMany(e => e.EventApplicationUsers)
                .HasForeignKey(e => e.ApplicationUserId);//.OnDelete(DeleteBehavior.Cascade);
        }
    }
}
