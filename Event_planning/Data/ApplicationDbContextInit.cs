﻿using Event_planning.Models;
using Event_planning.Models.ForEventDataModels;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication;
using System.Linq;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace Event_planning.Data
{
    public class ApplicationDbContextInit
    {
        public static void Initialize(ApplicationDbContext context)
        {
            try
            {
                //context.Users.RemoveRange(context.Users);
                //context.Roles.RemoveRange(context.Roles);
                //contextDb.OtherEventInfos.RemoveRange(contextDb.OtherEventInfos);
                //context.Events.RemoveRange(context.Events);
                //context.SaveChanges();

                if (!context.Roles.Any())
                {
                    string adminRoleName = "sysadmin";
                    string userRoleName = "user";
                    IdentityRole adminRole = new IdentityRole { Name = adminRoleName, NormalizedName = adminRoleName.ToUpper() };
                    IdentityRole userRole = new IdentityRole { Name = userRoleName, NormalizedName = userRoleName.ToUpper() };
                    context.Roles.Add(userRole);
                    context.Roles.Add(adminRole);
                    context.SaveChanges();
                }

                if (!context.Users.Any())
                {
                    ApplicationUser u1 = new ApplicationUser()
                    {
                        Email = "temp@gmail.com",
                        UserName = "temp@gmail.com",
                        EmailConfirmed = true, 
                        PasswordHash = "",
                        Name="Evgen",
                        Age=30
                    };

                    ApplicationUser u2 = new ApplicationUser()
                    {
                        Email = "dase@p33.org",
                        UserName = "dase@p33.org",
                        EmailConfirmed = true,
                        PasswordHash = "",
                        Name = "Tolyan",
                        Age = 20
                    };

                    context.Users.AddRange(u1, u2);
                    context.SaveChanges();
                }

                if (!context.Events.Any())
                {
                Event e1 = new Event()
                {
                    Title = "Party",
                    DateTime = new System.DateTime(2017, 10, 10),
                    MaxUsersCount = 5,
                    CurUsersCount = 0
                    };

                    Event e2 = new Event()
                    {
                        Title = "Film",
                        DateTime = new System.DateTime(2017, 05, 12),
                        MaxUsersCount = 50,
                        CurUsersCount = 0
                    };

                    Event e3 = new Event()
                    {
                        Title = "Camping",
                        DateTime = new System.DateTime(2017, 01, 11),
                        MaxUsersCount = 15,
                        CurUsersCount = 0
                    };

                    context.Events.AddRange(new List<Event>() { e1, e2, e3 });
                    context.SaveChanges();

                    OtherEventInfo oei1 = new OtherEventInfo()
                    {
                        HeadInfo = "Location",
                        TextInfo = "Yakub Kolas Street",
                        EventId = e1.Id
                    };

                    OtherEventInfo oei2 = new OtherEventInfo
                    {
                        HeadInfo = "Dress code",
                        TextInfo = "yes",
                        EventId = e1.Id
                    };

                    OtherEventInfo oei3 = new OtherEventInfo
                    {
                        HeadInfo = "Genre",
                        TextInfo = "rock",
                        EventId = e2.Id
                    };

                    OtherEventInfo oei4 = new OtherEventInfo
                    {
                        HeadInfo = "Age limit",
                        TextInfo = "18+",
                        EventId = e3.Id
                    };

                    context.OtherEventInfos.AddRange(oei1, oei2, oei3, oei4);
                    context.SaveChanges();
                }
            }
            catch
            {
                Initialize(context);
            } 
        }
    }
}
