﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Event_planning.Models.ViewModels
{
    public class EventContext
    {
        public int Id { get; set; }
        [Required]
        public Event Event { get;set; }

        public IEnumerable<ForEventDataModels.OtherEventInfo> EventInfos { get; set; }

        public EventContext()
        {
            EventInfos = new List<ForEventDataModels.OtherEventInfo>();
        }
    }
}
