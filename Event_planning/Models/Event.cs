﻿using Event_planning.Models.ForEventDataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Event_planning.Models
{
    public class Event
    {
        public int Id { get; set; }
        [Required]
        public String Title { get; set; }
        [Required]
        public DateTime DateTime { get; set; }
        [Required]
        public int MaxUsersCount { get; set; }
        [Required]
        public int CurUsersCount { get; set; } = 0;

        public List<EventApplicationUser> EventApplicationUsers { get; set; }

        public List<OtherEventInfo> OtherEventInfos { get; set; }

        public Event()
        {
           EventApplicationUsers = new List<EventApplicationUser>();
            OtherEventInfos = new List<OtherEventInfo>();
        }
    }
}
