﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Event_planning.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public String Name { get; set; }
        [Required]
        public int Age { get; set; }

        public List<EventApplicationUser> EventApplicationUsers { get; set; }

        public ApplicationUser()
        {
            EventApplicationUsers = new List<EventApplicationUser>();
        }
    }
}
