﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Event_planning.Models.ForEventDataModels
{
    public class OtherEventInfo
    {
        public int Id { get; set; }

        [Required]
        public string HeadInfo { get; set; }

        [Required]
        public string TextInfo { get; set; }

        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}
