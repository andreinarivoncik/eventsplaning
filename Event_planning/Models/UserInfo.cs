﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Event_planning.Models
{
    public class UserInfo
    {
        public int Id { get; set; }

        [Required]
        public String FirstName { get; set; }
        [Required]
        public int UserAge { get; set; }

        public int ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
